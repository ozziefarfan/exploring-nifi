# Exploring NiFi
The goal of this project is to learn and explore the usage of Apache NiFi using docker.

## Apache NiFi - What is it?
(From nifi.apache.org )
Apache NiFi supports powerful and scalable directed graphs of data routing, transformation, and system mediation logic. Some of the high-level capabilities and objectives of Apache NiFi include:

- Web-based user interface
  - Seamless experience between design, control, feedback, and monitoring
- Highly configurable
  - Loss tolerant vs guaranteed delivery
  - Low latency vs high throughput
  - Dynamic prioritization
  -   Flow can be modified at runtime
  - Back pressure
- Data Provenance
  - Track dataflow from beginning to end
- Designed for extension
  - Build your own processors and more
  - Enables rapid development and effective testing
- Secure
  - SSL, SSH, HTTPS, encrypted content, etc...
  - Multi-tenant authorization and internal authorization/policy management

## Apache NiFi 

### Starting NiFi and NiFi Registry

Running NiFi on docker:
```
$ docker run --name nifi2 -p 8080:8080 -i -v ~/Learning/Nifi/shared-directory:/opt/nifi/nifi-current/ls-target apache/nifi
```

To start the version control feature, we need to run the NiFi Registry:

```
$ docker run --name nifi-registry -p 18080:18080 apache/nifi-registry
```

### Connecting NiFi and NiFi Registry

Connect to the services through your browser.  In 2 different tabs start:
* `http://localhost:8080/nifi`
* `http://localhost:18080/nifi-registry`

On the first tab (localhost:8080/nifi), on the top right menu, go to: 
   "Controller Settings" options >> "Registry Clients" tab
and add one entry to connect the registry instance 
* press the + Sign 
* add an entry with name "local nifi", and value: `http://localhost:18080/`

The last step is to create a "*Bucket*" which is just the holder for one or more versions of a *Process Groups*.  We need to have
at least one bucket on the nifi-registry to hold the nifi flows.

*To add the "Bucket"* go to the NiFi-Registry tab, select the tool (🔧) on the top right corner, this will pop-up a window, use that 
screen to enter the details of the bucket. 

### Loading sample template into NiFi
NiFi comes with a sample template under the directly ls-target.  To verify this we need to connect to the docker container running NiFi, and then verify that the files are in place.
```

$ docker exec -it nifi2 bash

# Login will change after bash runs inside the container
nifi@be3bd10ef1d6:/opt/nifi/nifi-current$ ls ls-target/

# output should look something like this
-rw-r--r-- 1 nifi nifi 43551 Oct 30 23:25 SimpleFlow-template.xml

```

If everything looks in place.  Next step is to add the template through the NiFi UI.

* On the left "Operate" card use the icon for "Import Template"
* select the "SimpleFlow" example
* On the top menu use the "Template" icon and drag into the main board. The "SimpleFlow" option will show on the list.
* Select that "SimpleFlow" option, and wait a minute or so for the template to load.


Work in progress...

